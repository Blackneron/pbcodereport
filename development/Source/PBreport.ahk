; Hide the script and do not display a tray icon.
#NoTrayIcon

; Set the script speed to the maximum.
SetBatchLines -1

; Specify the application version.
ApplicationVersion = 1.0

; Set the working directory.
SetWorkingDir, %A_ScriptDir%

; Specify the default input file.
InputFile = Not Specified

; ==============================================================================
; Specify and check the configuration file.
; ==============================================================================

; Specify the configuration file.
ConfigurationFile = %A_ScriptDir%\PBreport.ini

; Install the configuration file.
FileInstall, PBreport.ini, PBreport.ini, 0

; Check if the configuration file does not exist.
IfNotExist, %ConfigurationFile%
{

  ; Display a warning message.
  MsgBox, 48, PBcodereport %ApplVersion% - Error !, The configuration file '%ConfigurationFile%' does not exist !

  ; Exit the application.
  ExitApp
}

; ==============================================================================
; Read and check the path of the text application.
; ==============================================================================

; Get the path of the text application.
IniRead, TextApplication, %ConfigurationFile%, Configuration, TextApplication

; Check if the text application does not exist.
IfNotExist, %TextApplication%
{

  ; Display a warning message.
  MsgBox, 48, PBcodereport %ApplVersion% - Error !, The text application '%TextApplication%' does not exist !

  ; Exit the application.
  ExitApp
}

; ==============================================================================
; Read and check the path of the xml application.
; ==============================================================================

; Get the path of the xml application.
IniRead, XMLApplication, %ConfigurationFile%, Configuration, XMLApplication

; Check if the xml application does not exist.
IfNotExist, %XMLApplication%
{

  ; Display a warning message.
  MsgBox, 48, PBcodereport %ApplVersion% - Error !, The XML application '%XMLApplication%' does not exist !

  ; Exit the application.
  ExitApp
}

; ==============================================================================
; Read and check the path of the last input file.
; ==============================================================================

; Get the path of the last opened input file.
IniRead, LastInputFile, %ConfigurationFile%, Configuration, InputFile

; Check if the input file still exist.
IfExist, %LastInputFile%
{

  ; Use the last path as the default path.
  InputFile = %LastInputFile%
}

; ==============================================================================
; Specify and check the report folder.
; ==============================================================================

; Specify the report folder.
ReportFolder = %A_ScriptDir%\Reports

; Check if the report folder does not exist.
IfNotExist, %ReportFolder%
{

  ; Create the report folder.
  FileCreateDir, %ReportFolder%

  ; Check if the report folder still does not exist.
  IfNotExist, %ReportFolder%
  {

    ; Display a warning message.
    MsgBox, 48, PBcodereport %ApplVersion% - Error !, The report folder could not be created !

    ; Exit the application.
    ExitApp
  }
}

; ==============================================================================
; Specify the output files.
; ==============================================================================

; Specify the clean code file.
CleanCodeFile = %ReportFolder%\CleanCode.xml

; Specify the only code file.
OnlyCodeFile = %ReportFolder%\OnlyCode.xml

; Specify the method report file.
MethodReportFile = %ReportFolder%\Methods.txt

; Specify the variable report file.
VariableReportFile = %ReportFolder%\Variables.txt

; Specify the variable list file.
VariableListFile = %ReportFolder%\VariableList.txt

; Specify the statistic report file.
StatisticReportFile = %ReportFolder%\Statistics.txt

; ==============================================================================
; Install and check the background file.
; ==============================================================================

; Specify the background file name.
BackgroundFileName = PBreport.jpg

; Install the background image.
FileInstall, PBreport.jpg, PBreport.jpg, 1

; Check if the background image does not exist.
IfNotExist, %A_ScriptDir%\%BackgroundFileName%
{

  ; Display a warning message.
  MsgBox, 48, PBcodereport %ApplVersion% - Error !, The background image '%BackgroundFileName%' does not exist !

  ; Exit the application.
  ExitApp
}

; ==============================================================================
; Display the GUI.
; ==============================================================================

; Specify the general look of the GUI.
Gui, Color, cE1DED9
Gui, -Caption

; Add the gui background graphic.
Gui, Add, Picture, x0 y0, %A_ScriptDir%\%BackgroundFileName%

; Specify the fonts for the GUI (prefered font verdana) - Weight: bold.
Gui, Font, s8 bold, MS sans serif
Gui, Font, s8 bold, Arial
Gui, Font, s8 bold, Verdana

; Add the application version to the title.
Gui, Add, Text, x227 y30 w30 h14 c5d5d5d, %ApplicationVersion%

; Specify the font size for the buttons.
Gui, Font, s10

; Add the text buttons to the GUI.
Gui, Add, Text, x297 y74 w65 h16 gSelectInput Center c5d5d5d, Select
Gui, Add, Text, x297 y98 w65 h16 gStartProcess Center c5d5d5d, Start
Gui, Add, Text, x297 y122 w65 h16 gDeleteFiles Center c5d5d5d, Delete
Gui, Add, Text, x297 y146 w65 h16 gShowReports Center c5d5d5d, Show
Gui, Add, Text, x297 y170 w65 h16 gExit Center c5d5d5d, Exit

; Specify the normal font size.
Gui, Font, s8

; Get only the filename and directory from the selected path.
SplitPath, InputFile, InputFileName, InputFileDir

; Add the filename of the input file to the GUI.
Gui, Add, Text, x25 y75 w75 h14 c5d5d5d, Input File:
Gui, Add, Text, x100 y75 w180 h14 ccc0033 vInput gShowOriginalCode, %InputFileName%

; Add the show output links to the GUI.
Gui, Add, Text, x26 y98 w110 h16 gShowOriginalCode c5d5d5d, Original Code
Gui, Add, Text, x26 y116 w110 h16 gShowCleanCode c5d5d5d, Clean Code
Gui, Add, Text, x26 y134 w110 h16 gShowOnlyCode c5d5d5d, Only Code
Gui, Add, Text, x166 y98 w110 h16 gShowStatisticReport c5d5d5d, Statistics
Gui, Add, Text, x166 y116 w110 h16 gShowVariableReport c5d5d5d, Variables
Gui, Add, Text, x166 y134 w110 h16 gShowVariableList c5d5d5d, Variable List
Gui, Add, Text, x166 y152 w110 h16 gShowMethodReport c5d5d5d, Methods

; Add the progress bar to the GUI.
Gui, Add, Progress, x26 y216 w328 h16 vProgressBar

; Add the progress bar text to the GUI.
Gui, Add, Text, x26 y238 w328 h16 vProgressText Center c5d5d5d

; Show the GUI.
Gui, Show, w380 h280, PBcodereport %ApplVersion%

Return

; ==============================================================================
; Start the process.
; ==============================================================================

; Start the process.
StartProcess:

  ; Submit the values from the gui.
  Gui, Submit, NoHide

  ; Check if the input file exist.
  IfNotExist, %InputFile%
  {

    ; Display a warning message.
    MsgBox, 48, PBcodereport %ApplVersion% - Error !, The selected input file does not exist ! Please select a valid file !

    ; Show the select dialog.
    Goto SelectInput
  }

  ; The input file exist.
  Else
  {

    ; ==========================================================================
    ; Delete the previous files.
    ; ==========================================================================

    ; Delete the previous files.
    Gosub DeleteFiles

    ; ==========================================================================
    ; Count all the lines of the original file.
    ; ==========================================================================

    ; Initialize the variables.
    TotalLines = 0

    ; Loop through the lines of the original file.
    Loop, read, %InputFile%
    {

      ; Increase the total number of lines.
      TotalLines := TotalLines + 1
    }

    ; ==========================================================================
    ; Analyze the original file.
    ; ==========================================================================

    ; Initialize the variables.
    EndFunctionCounter = 0
    SelectCaseCounter = 0
    EndSelectCounter = 0
    FunctionCounter = 0
    ForEachCounter = 0
    ElseifCounter = 0
    EndSubCounter = 0
    MethodCounter = 0
    CommentLines = 0
    EndIfCounter = 0
    WhileCounter = 0
    BreakCounter = 0
    CommentLines = 0
    NextCounter = 0
    LoopCounter = 0
    ItemCounter = 0
    ElseCounter = 0
    WendCounter = 0
    QuitCounter = 0
    ExitCounter = 0
    CaseCounter = 0
    LineCounter = 0
    EmptyLines = 0
    DimCounter = 0
    SubCounter = 0
    EmptyLines = 0
    ForCounter = 0
    CodeLines = 0
    MiscLines = 0
    IfCounter = 0
    DoCounter = 0
    CodeLines = 0

    ; Initialize the clean code line counter.
    CleanCodeLines = 0

    ; Initialize the only code line counter.
    OnlyCodeLines = 0

    ; Loop through the lines of the original file.
    Loop, read, %InputFile%
    {

      ; Increase the line counter.
      LineCounter := LineCounter + 1

      ; ========================================================================
      ; Display the progress bar and text.
      ; ========================================================================

      ; Specify the number format for the output.
      SetFormat, float, 3.0

      ; Calculate the progress in percent.
      ProgressPercent := LineCounter / TotalLines * 100

      ; Actualize the progress bar.
      GuiControl,, ProgressBar, %ProgressPercent%

      ; Specify the step size for the progress bar text.
      ProgressStep := TotalLines / 100

      ; Check if the progress bar text needs to be actualized.
      If mod(LineCounter, ProgressStep) = 0 Or LineCounter = TotalLines
      {

        ; Actualize the progress bar text.
        GuiControl,, ProgressText, Analyzing Line %LineCounter% of %TotalLines% - %ProgressPercent%`%
      }

      ; ========================================================================
      ; Analyze each line.
      ; ========================================================================

      ; Disable the auto trim mode.
      AutoTrim, Off

      ; Get the actual line.
      ActualLine = %A_LoopReadLine%

      ; Enable the auto trim mode.
      AutoTrim, On

      ; ========================================================================
      ; Check for the product name.
      ; ========================================================================

      ; Check if the line contains a product name.
      IfInString, ActualLine, <BuildWinProductName>
      {

        ; Get the product name out of the actual line.
        StringReplace, BuildWinProductName, ActualLine, <BuildWinProductName>, , All
        StringReplace, BuildWinProductName, BuildWinProductName, </BuildWinProductName>, , All

        ; Trim the product name.
        BuildWinProductName = %BuildWinProductName%
      }

      ; ========================================================================
      ; Check for the company name.
      ; ========================================================================

      ; Check if the line contains a company name.
      IfInString, ActualLine, <BuildWinCompanyName>
      {

        ; Get the company name out of the actual line.
        StringReplace, BuildWinCompanyName, ActualLine, <BuildWinCompanyName>, , All
        StringReplace, BuildWinCompanyName, BuildWinCompanyName, </BuildWinCompanyName>, , All

        ; Trim the company name.
        BuildWinCompanyName = %BuildWinCompanyName%
      }

      ; ========================================================================
      ; Check for the windows application name.
      ; ========================================================================

      ; Check if the line contains a windows application name.
      IfInString, ActualLine, <BuildWinName>
      {

        ; Get the windows application name out of the actual line.
        StringReplace, BuildWinName, ActualLine, <BuildWinName>, , All
        StringReplace, BuildWinName, BuildWinName, </BuildWinName>, , All

        ; Trim the windows application name.
        BuildWinName = %BuildWinName%
      }

      ; ========================================================================
      ; Check for the application version.
      ; ========================================================================

      ; Check if the line contains a long version.
      IfInString, ActualLine, <LongVersion>
      {

        ; Get the long version out of the actual line.
        StringReplace, LongVersion, ActualLine, <LongVersion>, , All
        StringReplace, LongVersion, LongVersion, </LongVersion>, , All

        ; Trim the long version.
        LongVersion = %LongVersion%
      }

      ; ========================================================================
      ; Check for the release version.
      ; ========================================================================

      ; Check if the line contains a non release version.
      IfInString, ActualLine, <NonRelease>
      {

        ; Get the non release version out of the actual line.
        StringReplace, NonRelease, ActualLine, <NonRelease>, , All
        StringReplace, NonRelease, NonRelease, </NonRelease>, , All

        ; Trim the non release version.
        NonRelease = %NonRelease%
      }

      ; ========================================================================
      ; Check the REALbasic version.
      ; ========================================================================

      ; Check if the line contains a realbasic version.
      IfInString, ActualLine, <RBProject version="
      {

        ; Get the realbasic version out of the actual line.
        StringReplace, RealbasicVersion, ActualLine, <RBProject version=", , All
        StringReplace, RealbasicVersion, RealbasicVersion, ">, , All

        ; Trim the realbasic version.
        RealbasicVersion = %RealbasicVersion%
      }

      ; ========================================================================
      ; Check the line type - Comment line.
      ; ========================================================================

      ; Check the line type - Comment line.
      IfInString, ActualLine, <SourceLine>//
      {

        ; Increase the comment line counter.
        CommentLines := CommentLines + 1
      }

      ; ========================================================================
      ; Check the line type - Empty line.
      ; ========================================================================

      ; Check the line type - Empty line.
      Else IfInString, ActualLine, <SourceLine></SourceLine>
      {

        ; Increase the empty line counter.
        EmptyLines := EmptyLines + 1
      }

      ; ========================================================================
      ; Check the line type - Code line.
      ; ========================================================================

      ; Check the line type - Code line.
      Else IfInString, ActualLine, <SourceLine>
      {

        ; Increase the code line counter.
        CodeLines := CodeLines + 1
      }

      ; ========================================================================
      ; Check the line type - Miscellaneous line.
      ; ========================================================================

      ; Check the line type - Miscellaneous line.
      Else
      {

        ; Increase the misc line counter.
        MiscLines := MiscLines + 1
      }

      ; ========================================================================
      ; Check for methods.
      ; ========================================================================

      ; Check if the line is the begin of a method.
      IfInString, ActualLine, <Method>
      {

        ; Increase the method counter.
        MethodCounter := MethodCounter + 1

        ; Set the method flag.
        MethodFlag := 1
      }

      ; ========================================================================
      ; Check for items.
      ; ========================================================================

      ; Check if the line contains an item name.
      IfInString, ActualLine, <ItemName>
      {

        ; Increase the item counter.
        ItemCounter := ItemCounter + 1

        ; Get the item name out of the actual line.
        StringReplace, ItemName, ActualLine, <ItemName>, , All
        StringReplace, ItemName, ItemName, </ItemName>, , All

        ; Trim the item name.
        ItemName = %ItemName%

        ; Check if the actual item name is a method name.
        If MethodFlag = 1
        {

          ; Reset the method flag.
          MethodFlag := 0

          ; Add the item information to the method report output.
          MethodReportOutput = %MethodReportOutput%%ItemName%%A_Tab%%LineCounter%`n
        }
      }

      ; ========================================================================
      ; Check the elements - Sub.
      ; ========================================================================

      ;Check the elements - Sub.
      IfInString, ActualLine, <SourceLine>Sub%A_Space%
      {

        ; Increase the SubCounter.
        SubCounter := SubCounter + 1
      }

      ; ========================================================================
      ; Check the elements - End Sub.
      ; ========================================================================

      ;Check the elements - End Sub.
      Else IfInString, ActualLine, <SourceLine>End Sub
      {

        ; Increase the EndSubCounter.
        EndSubCounter := EndSubCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Function.
      ; ========================================================================

      ;Check the elements - Function.
      Else IfInString, ActualLine, <SourceLine>Function%A_Space%
      {

        ; Increase the FunctionCounter.
        FunctionCounter := FunctionCounter + 1
      }

      ; ========================================================================
      ; Check the elements - End Function.
      ; ========================================================================

      ;Check the elements - End Function.
      Else IfInString, ActualLine, <SourceLine>End Function
      {

        ; Increase the EndFunctionCounter.
        EndFunctionCounter := EndFunctionCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Dim.
      ; ========================================================================

      ;Check the elements - Dim.
      Else IfInString, ActualLine, <SourceLine>Dim%A_Space%
      {

        ; Increase the DimCounter.
        DimCounter := DimCounter + 1

        ; Cut the xml information from the actual line.
        StringReplace, VariableInfo, ActualLine, <SourceLine>Dim%A_Space%, , All
        StringReplace, VariableInfo, VariableInfo, </SourceLine>, , All

        ; Get the position of the 'As' characters.
        Position := InStr(VariableInfo, " As ")

        ; Get the variable name out of the string.
        VariableName := SubStr(VariableInfo, 1, Position - 1)

        ; Trim the variable name.
        VariableName = %VariableName%

        ; Get the variable type out of the string.
        VariableType := SubStr(VariableInfo, Position + 4)

        ; Trim the variable type.
        VariableType = %VariableType%

        ; Add the variable information to the variable output.
        VariableReportOutput = %VariableReportOutput%%VariableName%%A_Tab%%VariableType%%A_Tab%%ItemName%%A_Tab%%LineCounter%`n

        ; Check if there are more than one variable separated by comma.
        Position := InStr(VariableName, ",")

        ; Check if a comma was found.
        If Position > 0
        {

          ; Split the variable name string into different parts.
          StringSplit, NameArray, VariableName, `,

          ; Loop through the different names
          Loop, %NameArray0%
          {

            ; Specify the actual variable name.
            ActualName := NameArray%a_index%

            ; Get the position of the '(' character.
            Position := InStr(ActualName, "(")

            ; Check if a '(' character was found.
            If Position > 0
            {

              ; Get the variable name without the array size.
              ActualName := SubStr(ActualName, 1, Position - 1)
            }

            ; Trim the variable name.
            ActualName = %ActualName%

            ; Add the variable name to the variable list.
            VariableListOutput = %VariableListOutput%%ActualName%`n
          }
        }
        
        ; No comma was found.
        Else
        {

          ; Get the position of the '(' character.
          Position := InStr(VariableName, "(")

          ; Check if a '(' character was found.
          If Position > 0
          {

            ; Get the variable name without the array size.
            VariableName := SubStr(VariableName, 1, Position - 1)

            ; Trim the variable name.
            VariableName = %VariableName%
          }

          ; Add the variable name to the variable list.
          VariableListOutput = %VariableListOutput%%VariableName%`n
        }
      }

      ; ========================================================================
      ; Check the elements - If.
      ; ========================================================================

      ;Check the elements - If.
      Else IfInString, ActualLine, <SourceLine>If%A_Space%
      {

        ; Increase the IfCounter.
        IfCounter := IfCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Elseif.
      ; ========================================================================

      ;Check the elements - Elseif.
      Else IfInString, ActualLine, <SourceLine>Elseif
      {

        ; Increase the ElseifCounter.
        ElseifCounter := ElseifCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Else.
      ; ========================================================================

      ;Check the elements - Else.
      Else IfInString, ActualLine, <SourceLine>Else%A_Space%
      {

        ; Increase the ElseCounter.
        ElseCounter := ElseCounter + 1
      }

      ; ========================================================================
      ; Check the elements - End If.
      ; ========================================================================

      ;Check the elements - End If.
      Else IfInString, ActualLine, <SourceLine>End If
      {

        ; Increase the EndIfCounter.
        EndIfCounter := EndIfCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Select Case.
      ; ========================================================================

      ;Check the elements - Select Case.
      Else IfInString, ActualLine, <SourceLine>Select Case
      {

        ; Increase the SelectCaseCounter.
        SelectCaseCounter := SelectCaseCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Case.
      ; ========================================================================

      ;Check the elements - Case.
      Else IfInString, ActualLine, <SourceLine>Case%A_Space%
      {

        ; Increase the CaseCounter.
        CaseCounter := CaseCounter + 1
      }

      ; ========================================================================
      ; Check the elements - End Select.
      ; ========================================================================

      ;Check the elements - End Select.
      Else IfInString, ActualLine, <SourceLine>End Select
      {

        ; Increase the EndSelectCounter.
        EndSelectCounter := EndSelectCounter + 1
      }

      ; ========================================================================
      ; Check the elements - While.
      ; ========================================================================

      ;Check the elements - While.
      Else IfInString, ActualLine, <SourceLine>While%A_Space%
      {

        ; Increase the WhileCounter.
        WhileCounter := WhileCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Wend.
      ; ========================================================================

      ;Check the elements - Wend.
      Else IfInString, ActualLine, <SourceLine>Wend
      {

        ; Increase the WendCounter.
        WendCounter := WendCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Break.
      ; ========================================================================

      ;Check the elements - Break.
      Else IfInString, ActualLine, <SourceLine>Break
      {

        ; Increase the BreakCounter.
        BreakCounter := BreakCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Quit.
      ; ========================================================================

      ;Check the elements - Quit.
      Else IfInString, ActualLine, <SourceLine>Quit
      {

        ; Increase the QuitCounter.
        QuitCounter := QuitCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Exit.
      ; ========================================================================

      ;Check the elements - Exit.
      Else IfInString, ActualLine, <SourceLine>Exit
      {

        ; Increase the ExitCounter.
        ExitCounter := ExitCounter + 1
      }

      ; ========================================================================
      ; Check the elements - For Each.
      ; ========================================================================

      ;Check the elements - For Each.
      Else IfInString, ActualLine, <SourceLine>For Each%A_Space%
      {

        ; Increase the ForEachCounter.
        ForEachCounter := ForEachCounter + 1
      }

      ; ========================================================================
      ; Check the elements - For.
      ; ========================================================================

      ;Check the elements - For.
      Else IfInString, ActualLine, <SourceLine>For%A_Space%
      {

        ; Increase the ForCounter.
        ForCounter := ForCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Next.
      ; ========================================================================

      ;Check the elements - Next.
      Else IfInString, ActualLine, <SourceLine>Next%A_Space%
      {

        ; Increase the NextCounter.
        NextCounter := NextCounter + 1
      }

      ;Check the elements - Next.
      Else IfInString, ActualLine, <SourceLine>Next</SourceLine>
      {

        ; Increase the NextCounter.
        NextCounter := NextCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Do.
      ; ========================================================================

      ;Check the elements - Do.
      Else IfInString, ActualLine, <SourceLine>Do%A_Space%
      {

        ; Increase the DoCounter.
        DoCounter := DoCounter + 1
      }

      ; ========================================================================
      ; Check the elements - Loop.
      ; ========================================================================

      ;Check the elements - Loop.
      Else IfInString, ActualLine, <SourceLine>Loop
      {

        ; Increase the LoopCounter.
        LoopCounter := LoopCounter + 1
      }

      ; ========================================================================
      ; Create a clean code file.
      ; ========================================================================

      ; Disable the auto trim mode.
      AutoTrim, Off

      ; Check if the actual line does not contain - <SourceLine></SourceLine>
      IfNotInString, ActualLine, <SourceLine></SourceLine>
      {

        ; Check if the actual line does not contain - <SourceLine>//
        IfNotInString, ActualLine, <SourceLine>//
        {

          ; Check if the actual line contains - <SourceLine>
          IfInString, ActualLine, <SourceLine>
          {

            ; Get the position of the ' //' characters.
            Position := InStr(ActualLine, " //")

            ; If no position was found try without the space character.
            If Position = 0
            {

              ; Get the position of the '//' characters.
              Position := InStr(ActualLine, "//")
            }

            ; Check if there is a comment after to the code.
            If Position > 0
            {

              ; Cut the comment from the actual line.
              CleanLine := SubStr(ActualLine, 1, Position - 1)

              ; Add the </SourceLine>.
              CleanLine = %CleanLine%</SourceLine>
            }

            ; No comment was found.
            Else
            {

              ; Specify the clean line.
              CleanLine = %ActualLine%
            }

            ; Add the actual code line to the clean code output.
            CleanCodeOutput = %CleanCodeOutput%%CleanLine%`n

            ; Increase the clean code line counter.
            CleanCodeLines := CleanCodeLines + 1

            ; ==================================================================
            ; Create an only code file.
            ; ==================================================================

            ; Enable the auto trim mode.
            AutoTrim, On

            ; Get the code out of the actual line.
            StringReplace, OnlyCode, CleanLine, <SourceLine>, , All
            StringReplace, OnlyCode, OnlyCode, </SourceLine>, , All

            ; Trim the string.
            OnlyCode = %OnlyCode%

            ; Add the actual code line to the only code output.
            OnlyCodeOutput = %OnlyCodeOutput%%OnlyCode%`n

            ; Increase the only code line counter.
            OnlyCodeLines := OnlyCodeLines + 1
          }

          ; The line is a miscellaneous line.
          Else
          {

            ; Add the actual misc line to the code output.
            CleanCodeOutput = %CleanCodeOutput%%ActualLine%`n

            ; Increase the clean code line counter.
            CleanCodeLines := CleanCodeLines + 1
          }
        }
      }
    }

    ; ==========================================================================
    ; Write the clean code to a file.
    ; ==========================================================================

    ; Create the oputput file with the clean code.
    FileAppend, %CleanCodeOutput%, %CleanCodeFile%

    ; Free the RAM.
    CleanCodeOutput =

    ; ==========================================================================
    ; Write the only code to a file.
    ; ==========================================================================

    ; Create the oputput file with only the code.
    FileAppend, %OnlyCodeOutput%, %OnlyCodeFile%

    ; Free the RAM.
    OnlyCodeOutput =

    ; ==========================================================================
    ; Write the method report to a file.
    ; ==========================================================================

    ; Sort the method output.
    Sort, MethodReportOutput

    ; Add the title to the method report.
    Output = '========== Methods ==========%A_Tab%'==============`n`n

    ; Add the table header to the method report.
    Output = %Output%Method Name%A_Tab%Line Number`n

    ; Add the list to the method report.
    Output = %Output%%MethodReportOutput%

    ; Create the method report.
    FileAppend, %Output%, %MethodReportFile%

    ; Free the RAM.
    MethodReportOutput =

    ; ==========================================================================
    ; Write the variable report to a file.
    ; ==========================================================================

    ; Sort the variable output.
    Sort, VariableReportOutput

    ; Add the title to the variable report.
    Output = '=============== Variables ===============%A_Tab%'========================%A_Tab%'========================%A_Tab%'==============`n`n

    ; Add the table header to the variable report.
    Output = %Output%Variable Name%A_Tab%Variable Type%A_Tab%Item Name%A_Tab%Line Number`n

    ; Add the list to the variable report.
    Output = %Output%%VariableReportOutput%

    ; Create the variable report.
    FileAppend, %Output%, %VariableReportFile%

    ; Free the RAM.
    VariableReportOutput =
    Output =

    ; ==========================================================================
    ; Write the temporary variable list to a file.
    ; ==========================================================================

    ; Sort the variable list.
    Sort, VariableListOutput, U

    ; Save the variable list.
    FileAppend, %VariableListOutput%, %VariableListFile%

    ; Free the RAM.
    VariableListOutput =

    ; ==========================================================================
    ; Count the variables.
    ; ==========================================================================

    ; Initialize the total variables counter.
    TotalVariables = 0

    ; Initialize the line counter.
    LineCounter = 0

    ; Loop through the lines of the variable list.
    Loop, read, %VariableListFile%
    {

      ; Count all the variables from the list.
      TotalVariables := TotalVariables + 1
    }

    ; Loop through the variable list.
    Loop, read, %VariableListFile%
    {

      ; Increase the line counter.
      LineCounter := LineCounter + 1

      ; ========================================================================
      ; Display the progress bar and text.
      ; ========================================================================

      ; Specify the number format for the output.
      SetFormat, float, 3.0

      ; Calculate the progress in percent.
      ProgressPercent := LineCounter / TotalVariables * 100

      ; Actualize the progress bar.
      GuiControl,, ProgressBar, %ProgressPercent%

      ; Specify the step size for the progress bar text.
      ProgressStep := TotalVariables / 100

      ; Check if the progress bar text needs to be actualized.
      If mod(LineCounter, ProgressStep) = 0 Or LineCounter = TotalVariables
      {

        ; Actualize the progress bar text.
        GuiControl,, ProgressText, Count Variable %LineCounter% of %TotalVariables% - %ProgressPercent%`%
      }

      ; ========================================================================
      ; Count the variables.
      ; ========================================================================

      ; Check if the variable is a new variable.
      If A_LoopReadLine <> ActualVariable
      {

        ; Initialize the variable counter.
        VariableCounter = 0

        ; Get the actual variable.
        ActualVariable = %A_LoopReadLine%

        ; Loop through the lines of the only code file.
        Loop, read, %OnlyCodeFile%
        {

          ; Get the actual line.
          ActualLine = %A_LoopReadLine%

          ; Check if the actual line is not a variable definition.
          IfNotInString, ActualLine, Dim%A_Space%
          {

            ; Specify the search pattern for the variables.
            Pattern := "(^|[ \\\-+*/:(),.=&]) {0,1}" . ActualVariable . " {0,1}($|[\\\-+*/(),.=&])"

            ; Rplace all variables that match the pattern.
            Result := RegExReplace(ActualLine, Pattern, "", ReplacementCount)

            ; Increase the variable counter.
            VariableCounter := VariableCounter + ReplacementCount
          }
        }

        ; Add the variable information to the variable list output.
        VariableListOutput = %VariableListOutput%%ActualVariable%%A_Tab%%VariableCounter%`n
      }
    }

    ; ==========================================================================
    ; Write the new variable list to a file.
    ; ==========================================================================

    ; Add the title to the variable list.
    Output = '========== Variable List ==========%A_Tab%'==========`n`n

    ; Add the table header to the variable list.
    Output = %Output%Variable Name%A_Tab%Quantity`n

    ; Add the list.
    Output = %Output%%VariableListOutput%

    ; Delete the old variable list.
    FileDelete, %VariableListFile%

    ; Create the variable list.
    FileAppend, %Output%, %VariableListFile%

    ; Free the RAM.
    VariableListOutput =
    Output =

    ; ==========================================================================
    ; Create the statistics report.
    ; ==========================================================================

    ; Specify the number format for the output.
    SetFormat, float, 6.2

    ; ==========================================================================
    ; Create the project information.
    ; ==========================================================================

    ; Add the title to the statistics report.
    StatisticReportOutput = '========== Project Information ==========%A_Tab%'============================%A_Tab%'========%A_Tab%'========`n`n

    ; Add the product name to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Product Name%A_Tab%%BuildWinProductName%`n

    ; Add the company name to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Company Name%A_Tab%%BuildWinCompanyName%`n

    ; Add the windows application name to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Application Name%A_Tab%%BuildWinName%`n

    ; Add the long version to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Long Version%A_Tab%%LongVersion%`n

    ; Add the non release version to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Non Release Version%A_Tab%%NonRelease%`n

    ; Add the real basic version to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%REALbasic Version%A_Tab%%RealbasicVersion%`n`n

    ; ==========================================================================
    ; Create the line statistics.
    ; ==========================================================================

    ; Add the title to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%'============ Line Statistics =============%A_Tab%'============================%A_Tab%'========%A_Tab%'========`n`n

    ; Add the table header to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Type%A_Tab%Number%A_Tab%Percent%A_Tab%Ratio`n

    ; Calculate the for the developer relevant number of lines.
    DeveloperLines := CommentLines + CodeLines + EmptyLines

    ; Calculate the line allocation in percent.
    CodeLinesPercent := CodeLines / DeveloperLines * 100
    CommentLinesPercent := CommentLines / DeveloperLines * 100
    EmptyLinesPercent := EmptyLines / DeveloperLines * 100

    ; Calculate the comment line / code line ratio.
    CommentCodeRatio := CommentLines / CodeLines

    ; Calculate the empty line / code line ratio.
    EmptyCodeRatio := EmptyLines / CodeLines

    ; Add the code lines to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Code Lines%A_Tab%%CodeLines%%A_Tab%%CodeLinesPercent%%A_Tab%1.0`n

    ; Add the comment lines to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Comment Lines%A_Tab%%CommentLines%%A_Tab%%CommentLinesPercent%%A_Tab%%CommentCodeRatio%`n

    ; Add the empty lines to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Empty Lines%A_Tab%%EmptyLines%%A_Tab%%EmptyLinesPercent%%A_Tab%%EmptyCodeRatio%`n

    ; Add the developper lines to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Code + Comment + Empty%A_Tab%%DeveloperLines%%A_Tab%100.00`n`n

    ; Add the number of miscellaneous lines to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Misc Lines%A_Tab%%MiscLines%`n

    ; Add the total number of lines to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Total Lines%A_Tab%%TotalLines%`n`n

    ; ==========================================================================
    ; Create the element statistics I.
    ; ==========================================================================

    ; Add the title to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%'========== Element Statistics I ==========%A_Tab%'============================%A_Tab%'========%A_Tab%'========`n`n

    ; Add the table header to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Element Name%A_Tab%Number`n

    ; Add the number of elements - Items.
    StatisticReportOutput = %StatisticReportOutput%Items%A_Tab%%ItemCounter%`n

    ; Add the number of elements - Methods.
    StatisticReportOutput = %StatisticReportOutput%Methods%A_Tab%%MethodCounter%`n

    ; Add the number of elements - Variables.
    StatisticReportOutput = %StatisticReportOutput%Variables%A_Tab%%TotalVariables%`n`n

    ; ==========================================================================
    ; Create the element statistics II.
    ; ==========================================================================

    ; Add the title to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%'========== Element Statistics II ==========%A_Tab%'============================%A_Tab%'========%A_Tab%'========`n`n

    ; Add the table header to the statistics output.
    StatisticReportOutput = %StatisticReportOutput%Element Name%A_Tab%Number`n

    ; Add the number of elements - Break.
    StatisticReportOutput = %StatisticReportOutput%Break%A_Tab%%BreakCounter%`n

    ; Add the number of elements - Dim.
    StatisticReportOutput = %StatisticReportOutput%Dim%A_Tab%%DimCounter%`n

    ; Add the number of elements - Do.
    StatisticReportOutput = %StatisticReportOutput%Do%A_Tab%%DoCounter%`n

    ; Add the number of elements - Elseif.
    StatisticReportOutput = %StatisticReportOutput%Elseif%A_Tab%%ElseifCounter%`n

    ; Add the number of elements - End Function.
    StatisticReportOutput = %StatisticReportOutput%End Function%A_Tab%%EndFunctionCounter%`n

    ; Add the number of elements - End If.
    StatisticReportOutput = %StatisticReportOutput%End If%A_Tab%%EndIfCounter%`n

    ; Add the number of elements - End Select.
    StatisticReportOutput = %StatisticReportOutput%End Select%A_Tab%%EndSelectCounter%`n

    ; Add the number of elements - End Sub.
    StatisticReportOutput = %StatisticReportOutput%End Sub%A_Tab%%EndSubCounter%`n

    ; Add the number of elements - Exit.
    StatisticReportOutput = %StatisticReportOutput%Exit%A_Tab%%ExitCounter%`n

    ; Add the number of elements - For Each.
    StatisticReportOutput = %StatisticReportOutput%For Each%A_Tab%%ForEachCounter%`n

    ; Add the number of elements - For.
    StatisticReportOutput = %StatisticReportOutput%For%A_Tab%%ForCounter%`n

    ; Add the number of elements - Function.
    StatisticReportOutput = %StatisticReportOutput%Function%A_Tab%%FunctionCounter%`n

    ; Add the number of elements - If.
    StatisticReportOutput = %StatisticReportOutput%If%A_Tab%%IfCounter%`n

    ; Add the number of elements - Loop.
    StatisticReportOutput = %StatisticReportOutput%Loop%A_Tab%%LoopCounter%`n

    ; Add the number of elements - Next.
    StatisticReportOutput = %StatisticReportOutput%Next%A_Tab%%NextCounter%`n

    ; Add the number of elements - Quit.
    StatisticReportOutput = %StatisticReportOutput%Quit%A_Tab%%QuitCounter%`n

    ; Add the number of elements - Select Case.
    StatisticReportOutput = %StatisticReportOutput%Select Case%A_Tab%%SelectCaseCounter%`n

    ; Add the number of elements - Sub.
    StatisticReportOutput = %StatisticReportOutput%Sub%A_Tab%%SubCounter%`n

    ; Add the number of elements - Wend.
    StatisticReportOutput = %StatisticReportOutput%Wend%A_Tab%%WendCounter%`n

    ; Add the number of elements - While.
    StatisticReportOutput = %StatisticReportOutput%While%A_Tab%%WhileCounter%`n

    ; ==========================================================================
    ; Write the statistics report to a file.
    ; ==========================================================================

    ; Create the statistics report.
    FileAppend, %StatisticReportOutput%, %StatisticReportFile%

    ; Free the RAM.
    StatisticReportOutput =

    ; ==========================================================================
    ; Reset the progress bar and text.
    ; ==========================================================================

    ; Reset the progress bar.
    GuiControl,, CodeProgress, 0

    ; Reset the progress bar text.
    GuiControl,, ProgressText, Process finished successfully !
  }

Return

; ==============================================================================
; Show the original code.
; ==============================================================================

ShowOriginalCode:

  ; Check if the original code file exist.
  IfExist, %InputFile%
  {

    ; Display the original code file.
    Run, %XMLApplication% "%InputFile%"
  }

Return

; ==============================================================================
; Show the clean code.
; ==============================================================================

ShowCleanCode:

  ; Check if the clean code file exist.
  IfExist, %CleanCodeFile%
  {

    ; Display the clean code file.
    Run, %XMLApplication% "%CleanCodeFile%"
  }

Return

; ==============================================================================
; Show the only code.
; ==============================================================================

ShowOnlyCode:

  ; Check if the only code file exist.
  IfExist, %OnlyCodeFile%
  {

    ; Display the only code file.
    Run, %XMLApplication% "%OnlyCodeFile%"
  }

Return

; ==============================================================================
; Show the method report.
; ==============================================================================

ShowMethodReport:

  ; Check if the method report exist.
  IfExist, %MethodReportFile%
  {

    ; Display the method report.
    Run, %TextApplication% "%MethodReportFile%"
  }

Return

; ==============================================================================
; Show the variable report.
; ==============================================================================

ShowVariableReport:

  ; Check if the variable report exist.
  IfExist, %VariableReportFile%
  {

    ; Display the variable report.
    Run, %TextApplication% "%VariableReportFile%"
  }

Return

; ==============================================================================
; Show the variable list.
; ==============================================================================
ShowVariableList:

  ; Check if the variable list exist.
  IfExist, %VariableListFile%
  {

    ; Display the variable list.
    Run, %TextApplication% "%VariableListFile%"
  }

Return

; ==============================================================================
; Show the statistic report.
; ==============================================================================
ShowStatisticReport:

  ; Check if the statistic report exist.
  IfExist, %StatisticReportFile%
  {

    ; Display the statistic report.
    Run, %TextApplication% "%StatisticReportFile%"
  }

Return

; ==============================================================================
; Show the report folder.
; ==============================================================================

; Show the report folder.
ShowReports:

  ; Open the report folder.
  Run, Explorer.exe %ReportFolder%

Return

; ==============================================================================
; Select the input file.
; ==============================================================================

; Select the input file manually.
SelectInput:

  ; Let the user choose an input file.
  FileSelectFile, InputFileNew, 1, ::{20d04fe0-3aea-1069-a2d8-08002b30309d}, Please select an input file !, REALbasic Project File (*.xml)

  ; Check if the input file exist.
  IfExist, %InputFileNew%
  {

    ; Specify the input file.
    InputFile = %InputFileNew%

    ; Get only the filename and directory from the selected path.
    SplitPath, InputFile, InputFileName, InputFileDir

    ; Display the input file name.
    GuiControl,, Input, %InputFileName%

    ; Check if the configuration file exist.
    IfExist, %ConfigurationFile%
    {

      ; Write the path of the selected input file to the configuration file.
      IniWrite, %InputFile%, %ConfigurationFile%, Configuration, InputFile
    }
  }

  ; The input file does not exist.
  Else
  {

    ; Display a warning message.
    MsgBox, 48, PBcodereport %ApplVersion% - Error !, The selected input file does not exist ! Please select a valid file !
  }

Return

; ==============================================================================
; Delete the existing files.
; ==============================================================================

; Delete the existing files.
DeleteFiles:

  ; Check if the clean code file exist.
  IfExist, %CleanCodeFile%
  {

    ; Delete the clean code file.
    FileDelete, %CleanCodeFile%
  }

  ; Check if the only code file exist.
  IfExist, %OnlyCodeFile%
  {

    ; Delete the only code file.
    FileDelete, %OnlyCodeFile%
  }

  ; Check if the method report file exist.
  IfExist, %MethodReportFile%
  {

    ; Delete the method report file.
    FileDelete, %MethodReportFile%
  }

  ; Check if the variable report file exist.
  IfExist, %VariableReportFile%
  {

    ; Delete the variable report file.
    FileDelete, %VariableReportFile%
  }

  ; Check if the variable list file exist.
  IfExist, %VariableListFile%
  {

    ; Delete the variable list file.
    FileDelete, %VariableListFile%
  }

  ; Check if the statistic report file exist.
  IfExist, %StatisticReportFile%
  {

    ; Delete the statistic report file.
    FileDelete, %StatisticReportFile%
  }

Return

; ==============================================================================
; Close the GUI and exit the application.
; ==============================================================================

; Exit the application.
GuiClose:

  ; Exit the application.
  Goto Exit

; ==============================================================================
; Exit the application
; ==============================================================================
Exit:

  ; Exit the application
  ExitApp
