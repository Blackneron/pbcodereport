# PBcodereport - README #
---

### Overview ###

The **PBcodereport** tool analyzes a selected REALbasic file and generates statistics data about methods, variables and more.

### Screenshots ###

![PBcodereport - GUI](development/readme/pbcodereport1.png "PBcodereport - GUI")

![PBcodereport - Method list](development/readme/pbcodereport2.png "PBcodereport - Method list")

![PBcodereport - Statistics](development/readme/pbcodereport3.png "PBcodereport - Statistics")

![PBcodereport - Variable list](development/readme/pbcodereport4.png "PBcodereport - Variable list")

### Setup ###

* Start the **PBreport.exe** executable with a doubleclick.
* Press the button **Select** to open the file selection window.
* Choose a **REALbasic** XML source code file and press **Open**.
* Press the button **Start** to analyze the source file.
* After the process has finished press the button **Show**.
* The folder with the created statistics files will be opened.
* You can import the files into **Microsoft Excel**.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBcodereport** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
